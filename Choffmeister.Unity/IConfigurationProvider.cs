﻿using System;

namespace Choffmeister.Unity
{
    public interface IConfigurationProvider : IDisposable
    {
        string GetConnectionString(string name, bool throwIfNotExistent = true);

        string GetSettingString(string key, bool throwIfNotExistent = true);
    }
}