﻿using System;
using System.Configuration;

namespace Choffmeister.Unity
{
    public class FileConfigurationProvider : IConfigurationProvider
    {
        public string GetConnectionString(string name, bool throwIfNotExistent = true)
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings[name];

            if (connectionString == null && throwIfNotExistent)
            {
                throw new Exception(string.Format("Mandatory connection string '{0}' not existent", name));
            }

            return connectionString != null ? connectionString.ConnectionString : null;
        }

        public string GetSettingString(string key, bool throwIfNotExistent = true)
        {
            string settingString = ConfigurationManager.AppSettings[key];

            if (settingString == null && throwIfNotExistent)
            {
                throw new Exception(string.Format("Mandatory setting string '{0}' not existent", key));
            }

            return settingString;
        }

        public void Dispose()
        {
        }
    }
}