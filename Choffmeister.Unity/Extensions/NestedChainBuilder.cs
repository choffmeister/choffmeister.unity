﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.ObjectBuilder;

namespace Choffmeister.Unity.Extensions
{
    public class NestedChainBuilder<TInterface> : UnityContainerExtension
        where TInterface : class
    {
        private readonly Action<TInterface, TInterface> _linkDelegate;
        private readonly List<Type> _types;

        public NestedChainBuilder(Action<TInterface, TInterface> linkDelegate, params Type[] types)
        {
            _linkDelegate = linkDelegate;
            _types = types.ToList();
        }

        protected override void Initialize()
        {
            this.Container.RegisterType(typeof(TInterface), _types[0]);

            this.Context.Strategies.Add(new NestedChainBuilderStrategy<TInterface>(_linkDelegate, _types), UnityBuildStage.PreCreation);
        }
    }

    public class NestedChainBuilderStrategy<TInterface> : BuilderStrategy
        where TInterface : class
    {
        private readonly Action<TInterface, TInterface> _linkDelegate;
        private readonly List<Type> _types;

        public NestedChainBuilderStrategy(Action<TInterface, TInterface> linkDelegate, List<Type> types)
        {
            _linkDelegate = linkDelegate;
            _types = types;
        }

        public override void PreBuildUp(IBuilderContext context)
        {
            NamedTypeBuildKey key = context.OriginalBuildKey;

            if (key.Type == typeof(TInterface))
            {
                TInterface current = null;

                for (int i = _types.Count - 1; i >= 0; i--)
                {
                    TInterface successor = current;
                    current = (TInterface)context.NewBuildUp(new NamedTypeBuildKey(_types[i]));
                    _linkDelegate(current, successor);
                }

                context.Existing = current;
                context.BuildComplete = true;
            }
        }
    }
}